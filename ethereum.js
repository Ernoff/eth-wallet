'use strict';

if (process.env.NODE_ENV != 'production') {
    require('dotenv').config();
}

const Web3 = require('web3');
let web3;
if (typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
  } else {
    // set the provider you want from Web3.providers
    web3 = new Web3(new Web3.providers.HttpProvider(process.env.uri));
  }

module.exports = web3;