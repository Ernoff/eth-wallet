'use strict';

if (process.env.NODE_ENV != 'production') {
    require('dotenv').config();
}

const redis = require('redis');
const bluebird = require('bluebird');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const client = redis.createClient(process.env.redis_port, process.env.redis_host);
client.on('error', (err) => {
  return console.error("[REDIS] Error encountered", err);
});

module.exports = client;