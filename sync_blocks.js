'use strict';

if (process.env.NODE_ENV != 'production') {
    require('dotenv').config();
}

const web3 = require("./ethereum");

async function sync_blocks(current_block_number, opts) {
  let latest_block_number = await web3.eth.getBlockNumber();
  let synced_block_number = await sync_to_block(current_block_number, latest_block_number, opts);

  web3.eth.subscribe('newBlockHeaders', (error, result) => error && console.log(error))
  .on('data', async function(blockHeader) {
    return await process_block(blockHeader.number, opts);
  })

  return synced_block_number;
}

async function process_block(block_hash_or_id, opts) {
  const block = await web3.eth.getBlock(block_hash_or_id, true);
  opts.onTransactions ? opts.onTransactions(block.transactions) : null;

  opts.onBlock ? opts.onBlock(block_hash_or_id) : null;

  return block;

}

async function sync_to_block(index, latest, opts) {
  if (index >= latest) {
    return index;
  } 

  await process_block(index +1, opts);
  return await sync_to_block(index + 1, latest, opts);
}

module.exports = sync_blocks;