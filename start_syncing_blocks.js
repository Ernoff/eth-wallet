'use strict';

if (process.env.NODE_ENV != 'production') {
    require('dotenv').config();
}

const web3 = require('web3');
const redis = require('./redis');
const queue = require('./queue');
const sync_blocks = require('./sync_blocks');


async function start_syncing_blocks() {
  let last_block_number = await redis.getAsync('eth:last-block')
  last_block_number = last_block_number || 0;

  sync_blocks(last_block_number, {
    onBlock: update_block_head,

    onTransactions: async (transactions) => {
      for (let transaction in transactions) {
        await process_transaction(transactions[transaction]);
      }
    }
  })
}

async function update_block_head(head) {
  return await redis.setAsync('eth:last-block', head);
}

async function process_transaction(transaction) {
  const address = transaction.to.toLowerCase();
  const amount_in_ether = web3.utils.fromWei(transaction.value);
  
  const watched_address = await redis.existAsync(`eth:address:public:${address}`)
  if (watched_address !== 1) {
    return false;
  }

  const transaction_exists = await redis.existAsync(`eth:address:public:${address}`)
  if(transaction_exists === 1) {
    return false;
  }

  const data = await redis.getAsync(`eth:address:public:${address}`)
  let addr_data = JSON.parse(data);
  addr_data[transaction.hash] = {
    value: amount_in_ether
  }

  await redis.setAsync(`eth:address:public:${address}`, JSON.stringify(addr_data))
  await redis.setAsync(`eth:transaction:${transaction.hash}`, transaction)

  //move funds to a cold wallet address
  // const cold_txid = await move_to_cold_storage(address, amount_in_ether)

  await queue_producer.send('transaction', [{
    txid: transaction.hash,
    value: amount_in_ether,
    to: transaction.to,
    from: transaction.from,
    // cold_txid: cold_txid
  }]);

  return true;
}

module.exports = start_syncing_blocks;