'use strict';

if (process.env.NODE_ENV != 'production') {
    require('dotenv').config();
}

const kafka = require('kafka-node');

const default_options = {
  kafkaHost: process.env.kafka_zookeeper_uri,
  autoConnect: true,
  fromOffset: 'earliest'
};
const client = new kafka.KafkaClient(default_options);

const producer = new kafka.Producer(client);

async function on_ready() {
  producer.on('ready', () => {
    console.log('ready')
  })
  .on('error', (err) => {
    console.log('error', err)
  })
}

console.log(process.env.kafka_zookeeper_uri)
const admin = new kafka.Admin(client);
admin.listTopics((err, res) => {
  console.log('topics', res)
  console.log('errors', err)
})
module.exports.consumer = (group_id = "ethereum_wallet_manager_consumer", topics = [], opts = {}) => {
  const options = Object.assign({groupId: group_id}, default_options, opts);
  const consumer = new kafka.ConsumerGroup(options, topics);
  return consumer;
}

async function send(topic, messages) {
  return new Promise((resolve, reject) => {
    messages = messages.map(JSON.stringify);
    producer.send([{topic, messages}], function (err, data) {
      console.log(err, data)
      if (err) return reject(err);
      resolve(data);
    })
  })
}

module.exports.client = client;
module.exports.on_ready = on_ready;
module.exports.send = send;
