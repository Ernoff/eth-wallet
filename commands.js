'use strict';

if (process.env.NODE_ENV != 'production') {
    require('dotenv').config();
}


const web3 = require("./ethereum");
const redis = require("./redis");
const queue = require("./queue");

async function listen_to_commands() {
  const queue_consumer = queue.consumer("eth.wallet.manager.commands", ['command']);
  queue_consumer.on('message', async function (topic_message) {
    try {
      const message = JSON.parse(topic_message.value);

      const resp = await create_address(message.meta);
      console.log('**************', resp)
      if (resp) {
        await queue.send('address.created', [resp]);
      }
    }
    catch (err) {
      console.error(topic_message, err)
      queue.send('errors', [{type: 'command', request: topic_message, error_code: err.code, error_message: err.message, error_stack: err.stack}])
    }
  })
  console.log("listening")
  return queue_consumer;
}

async function create_address(meta = {}) {
  const account = await web3.eth.accounts.create();
  const address = account.address.toLowerCase();
  await redis.setAsync(`eth:address:public:${address}`, JSON.stringify({}));

  await redis.setAsync(`eth:address:private:${address}`, account.privateKey);

  return Object.assign({}, meta, {address: account.address});
};

module.exports.listen_to_commands = listen_to_commands
