'use strict';

(async () => {

if (process.env.NODE_ENV != 'production') {
    require('dotenv').config();
}

const queue = require("./queue");
const commands = require("./commands");

let queue_consumer = commands.listen_to_commands()
await queue.send('command', ['create address']);

})();
